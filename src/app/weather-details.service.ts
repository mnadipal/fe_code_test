import { environment } from '../environments/environment';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';


/***
 * Constant keys are maintained at environment file
 ***/

@Injectable({
  providedIn: 'root'
})
export class WeatherDetailsService {
  // HttpClient is required for http requests
  constructor(private httpClient: HttpClient) { }

  getWeather(cityName) {
    // API call for getting weather details
    const url = `${environment.endpoint}${environment.api.forecast}${cityName}&appid=${environment.appid}`;
    return this.httpClient.get(url);
  }
  getForecast (cityName){
    // API call for getting forecast details
    const url = `${environment.endpoint}${environment.api.forecast}${cityName}&appid=${environment.appid}`;
    return this.httpClient.get(url);
  }
}
