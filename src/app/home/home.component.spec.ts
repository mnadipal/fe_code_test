import { WeatherDetailsService } from './../weather-details.service';
import { Router } from '@angular/router';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HomeComponent } from './home.component';

describe('HomeComponent', () => {
  let component: HomeComponent;
  let fixture: ComponentFixture<HomeComponent>;
  let router: Router;
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HomeComponent ],
      providers: [Router, WeatherDetailsService]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
  it('should have <h2> with "Weather Details"', () => {
    const bannerElement: HTMLElement = fixture.nativeElement;
    const h2 = bannerElement.querySelector('h2');
    expect(h2.textContent).toEqual('Weather Details');
  });
  it('should have <th> elements', () => {
    const bannerElement: HTMLElement = fixture.nativeElement;
    const td = bannerElement.querySelectorAll('th');
    expect(td[0]).toEqual('City');
    expect(td[1]).toEqual('Temperature');
    expect(td[2]).toEqual('Sunrise');
    expect(td[3]).toEqual('Sunset');
  });
  it(`should have citiesList'`, () => {
    const fixture = TestBed.createComponent(HomeComponent);
    const app = fixture.debugElement.componentInstance;
    const list = ['Venice', 'Paris', 'Brussels', 'Italy', 'IStanbul']
    expect(app.citiesList).toEqual(list);
  });
  it(`#getWeatherDetails should navigate to forecast page`, () => {
    const fixture = TestBed.createComponent(HomeComponent);
    const app = fixture.debugElement.componentInstance;
    
    let city = 'Italy';
    app.getWeatherDetails(city);

    const spy = router.navigate as jasmine.Spy;
    const navArgs = spy.calls.first().args[0];

  expect(navArgs).toBe('/weather-details/' + city, 'should nav to Weather Details for Italy');
  })
});
