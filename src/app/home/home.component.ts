import { WeatherDetailsService } from './../weather-details.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent {

  //Setting 5 preferred European cities
  citiesList = ['Venice', 'Paris', 'Brussels', 'Italy', 'IStanbul']
  cities = [];
  
  constructor(
    private router: Router,
    private service: WeatherDetailsService
  ) { 

    //To get weather details for 5 preferred cities
    for(let city in this.citiesList){
      this.service.getWeather(this.citiesList[city]).subscribe(res=> {
        this.cities.push(res);
      });
    }
  }

  //Navigate to weather details page upon clicking on a city(row)
  getWeatherDetails(city) {
    this.router.navigate(['/weather-details/'+ city.name]);
    console.log(city)
  }
}
