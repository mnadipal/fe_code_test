import { ActivatedRoute } from '@angular/router';
import { WeatherDetailsService } from './../weather-details.service';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WeatherDetailsComponent } from './weather-details.component';

describe('WeatherDetailsComponent', () => {
  let component: WeatherDetailsComponent;
  let fixture: ComponentFixture<WeatherDetailsComponent>;
  let route: ActivatedRoute;
  let city = 'Italy';
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WeatherDetailsComponent ],
      providers: [ ActivatedRoute, WeatherDetailsService]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WeatherDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
  it('should have <h2> with heading and city name', () => {
    const bannerElement: HTMLElement = fixture.nativeElement;
    const h2 = bannerElement.querySelector('h2');
    let cityName = 'Italy';
    expect(h2.textContent).toEqual(`Weather forecast for next 5 days at ${{cityName}}`);
  });
  it('should have <th> elements', () => {
    const bannerElement: HTMLElement = fixture.nativeElement;
    const td = bannerElement.querySelectorAll('th');
    expect(td[0]).toEqual('Date');
    expect(td[1]).toEqual('Temperature');
    expect(td[2]).toEqual('Sea Level');
  });
});
