import { element } from 'protractor';
import { ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { WeatherDetailsService } from '../weather-details.service';

@Component({
  selector: 'app-weather-details',
  templateUrl: './weather-details.component.html',
  styleUrls: ['./weather-details.component.scss']
})
export class WeatherDetailsComponent {

  forecastList = [];
  cityName = '';

  constructor(
    private service: WeatherDetailsService,
    private route: ActivatedRoute) { 

    // Get the selected  city name
    this.route.params.subscribe(res => {
      this.cityName = res['city'];
    })

    //Get the forcast details of selected city for next 5days
    this.service.getForecast(this.cityName).subscribe(res => {
      // To filter the 9'0clock records from all the existing records in the response
      res['list'].forEach(element => {
        if (element['dt_txt'].substr(11) === '09:00:00') {
          //To push 9'0 clock records into forecast list and display on UI
          this.forecastList.push(element);
        }
      });
    })
  }

}
